package com.example.miprimeraapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

                                                    // 3. implementar la interface OnClickListener
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    // 1. Boton del layout debe ser atributo en la clase
    Button botonRegistrarse, botonInicioSesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 2. asociar el atributo con el view del layout usando su id
        botonRegistrarse = (Button)findViewById(R.id.botonRegistrarse);
        botonInicioSesion = (Button)findViewById(R.id.botonInicioSesion);

        // 4. habilitar el botón para que sea clickeable
        botonRegistrarse.setOnClickListener(this);
        botonInicioSesion.setOnClickListener(this);
    }

    // 5. implementar el método onClick
    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.botonRegistrarse) {
            Intent i = new Intent(this,SignUpActivity.class);
            startActivity(i);
        } else if (view.getId() == R.id.botonInicioSesion) {
            Intent i = new Intent(this,LoginActivity.class);
            startActivity(i);
        }
    }
}