package com.example.miprimeraapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editUsuario, editContrasena;
    private Button botonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editUsuario = (EditText)findViewById(R.id.editUsuario);
        editContrasena = (EditText)findViewById(R.id.editContrasena);
        botonLogin = (Button)findViewById(R.id.buttonLogin);

        editUsuario.setOnClickListener(this);
        editContrasena.setOnClickListener(this);
        botonLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.botonRegistrarse) {
            Intent i = new Intent(this,SignUpActivity.class);
            startActivity(i);
        }

        if (view.getId() == R.id.buttonLogin) {
            if (usuario.equals(editUsuario.getText().toString())) {

                Intent i = new Intent(this,HomeActivity.class);
                startActivity(i);

            }
        }
    }

    private String usuario = "jorge";
    private String contraseña = "1234";
}